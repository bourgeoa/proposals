# proposals

> proposals for [solidcommunity.net](https://solidcommunity.net/) pod server

This repository is a work in progress.  

Currently, the aims are:

- **[New proposals](https://gitlab.com/bourgeoa/proposals/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=):** add a proposal
- **[History](https://gitlab.com/bourgeoa/proposals/issues):** Track current proposals

[**Create a new proposal →**](https://gitlab.com/bourgeoa/proposals/issues)

Support issues should be raised in the [support](https://gitlab.com/bourgeoa/support) repo